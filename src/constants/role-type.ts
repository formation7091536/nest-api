export enum RoleType {
  USER = 'USER',
  TEACHER = 'TEACHER',
  ADMIN = 'ADMIN',
}
